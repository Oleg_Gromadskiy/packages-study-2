import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Permission.location.request();
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Container(
            child: FutureBuilder<Position>(
              future: Geolocator.getCurrentPosition(),
              builder: (ctx, snapshot) => Text(snapshot.data.toString()),
            ),
          ),
        ),
      ),
    );
  }
}
